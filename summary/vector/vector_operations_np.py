import numpy as np

# Vector operations using numpy

def vector_add(v, w):
    """Adds corresponding elements"""
    return np.add(v, w)

def vector_subtract(v, w):
    """Subtracts corresponding elements"""
    return np.subtract(v,w)

def vector_sum(vectors):    
    """Sums all corresponding elements"""
    return np.sum(vectors, axis=0)  

def scalar_multiply(c, v):
    """C is a number, v is a vector"""
    return np.multiply(c,v)

def vector_mean(vectors):
    """Compute the vector whose ith element is the mean of the ith elements of the input vectors"""
    return np.mean(vectors, axis=0)

def dot(v, w):
    """v_1 * w_1 + ... + v_n * w_n"""
    return np.dot(v, w)

def sum_of_squares(v):
    """v_1 * v_1 + ... + v_n * v_n"""
    return dot(v, v)