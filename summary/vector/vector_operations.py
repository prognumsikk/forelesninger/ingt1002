# Vector operations

def vector_add(v, w): 
    """Adds corresponding elements""" 
    result = [] 
    for v_i, w_i in zip(v, w): 
        result.append(v_i + w_i) 
    return result

def vector_subtract(v, w):
    """Subtracts corresponding elements"""
    result = [] 
    for v_i, w_i in zip(v, w): 
        result.append(v_i - w_i) 
    return result


def vector_sum(vectors):
    """Sums all corresponding elements"""
    result = vectors[0] # Start with the first vector
    for vector in vectors[1:]: # Then loop over the others
        result = vector_add(result, vector) # And add them to the result
    return result


def scalar_multiply(c, v):
    """C is a number, v is a vector"""

    result = [] 
    for v_i in v: 
        result.append(c * v_i) 
    return result


def vector_mean(vectors):
    """Compute the vector whose ith element is the mean of the ith elements of the input vectors"""
    n = len(vectors)
    return scalar_multiply(1/n, vector_sum(vectors))


def dot(v, w): 
    """v_1 * w_1 + ... + v_n * w_n""" 
    result = 0 
    for v_i, w_i in zip(v, w): 
        result += v_i * w_i 
    return result


def sum_of_squares(v):
    """v_1 * v_1 + ... + v_n * v_n"""
    return dot(v, v)

