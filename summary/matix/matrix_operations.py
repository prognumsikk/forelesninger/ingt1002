import numpy as np

# Matrix operations using numpy


def matrix_add(A, B):
    """Adds corresponding elements"""
    return np.add(A, B)


def matrix_subtract(A, B):
    """Subtracts corresponding elements"""
    return np.subtract(A, B)


def marix_multiply(A, B):
    """Multiplies corresponding elements"""
    return np.multiply(A, B)


def matrix_sum(matrices):
    """Sums all corresponding elements"""
    result = matrices[0]  # Start with the first matrix
    for matrix in matrices[1:]:  # Then loop over the others
        result = matrix_add(result, matrix)  # And add them to the result
    return result


def matrix_scalar_multiply(c, A):
    """C is a number, A is a matrix"""
    return np.multiply(c, A)


def matrix_mean(matrices):
    """Compute the matrix whose ith element is the mean of the ith elements of the input matrices"""
    n = len(matrices)
    return matrix_scalar_multiply(1/n, matrix_sum(matrices))


def matrix_dot(A, B):
    """A_1 * B_1 + ... + A_n * B_n"""
    return np.dot(A, B)


def matrix_diagonal(A):
    """Returns the diagonal of A"""
    return np.diag(A)


def matrix_diagonal_dominate(A):
    """Returns True if A is diagonal dominate, False otherwise"""
    rows, cols = A.shape
    for i in range(rows):
        row_sum = 0
        for j in range(cols):
            if i != j:
                row_sum += abs(A[i, j])
        if abs(A[i, i]) < row_sum:
            return False
    return True
