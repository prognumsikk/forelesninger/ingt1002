# Assumptions

- Has access to Jupyter hub (NTNU).
- And/Or:
  - Installed Visual Studio Code
  - Installed extensions for
    - Python
    - Jupyter

# Chapter 1: Learning outcomes

- Markdown
  - Can distinuish between markup/markdown/html
  - Can create simple md-files
  
- Jupyter
  - Understand what ipynb-file is and can use md in jupyter files
  - Can write and execute simple Python code in jupyter
  - Understand what magic commands are and how to use them

- Python
  - Understand the basic of python (code-structure, comments, input, output)
  
  
# Topics

## Markdown

- Doc
  - [Reference](https://commonmark.org/help/)
  - [Markdown Guide](https://www.markdownguide.org/)
- Basics of formatting in markdown

## Jupyter

- Doc:
  - [Official docs site](https://docs.jupyter.org/en/latest/)
  - [Markdown in Jupyter](https://www.datacamp.com/tutorial/markdown-in-jupyter-notebook)
  - [Tutorials](https://www.datacamp.com/tutorial/tutorial-jupyter-notebook#whatisapp)

- Use of markdown in jupyter files
- Use of html
- Comments
- Code
- Magic commands

## Basic of Python

- Docs
  - [Python docs](https://docs.python.org/)
  - [IPython docs](https://ipython.readthedocs.io/en/stable/)

- comments: line comment or group comment
- grouping code using 'space'
- print: writing to console
- input: reading from the console
- case sensitivity
