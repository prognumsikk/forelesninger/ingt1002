---
marp: true
size: 4:3
paginate: true
---
<!-- https://marpit.marp.app/directives -->

# Syntaxrror

[Les mer om syntax error her](https://clouddevelop.org/syntax-error/)

Syntaksfeil i Python refererer til feil i programmeringsstrukturen og -formateringen som hindrer at koden kjører riktig.

Selv de beste programmerere gjør av og til feil. Men en erfaren programmerer
- finner oftest raskere ut av feilene
- klarer å rette dem

---
Viktig del av veien til å bli en god programmerer:
- bli flinkere til å forstå og korrigere feil. 

Her starter vi med den enkleste typen feil
- Syntaksfeil (SyntaxError) 
- betyr at koden bryter med Pythons grammatikkregler

---

# NameError
NameError (navnefeil) i Python skjer ved bruk av et navn som ikke er definert. Det kan være
- navn på variabel
- navn på funksjon
- navn på modul, klasse, ...
- eller noe som ikke var ment som et navn
    - men Python-tolkeren tror det er navn pga. andre tabber i koden vår.
