---
marp: true
size: 4:3
paginate: true
---
<!-- https://marpit.marp.app/directives -->

# Streng formattering

[Les mer om streng formattering her](https://realpython.com/python-string-formatting/)

Det finnes flere metoder for å formatere tekst:

1. “Old Style” String Formatting (% Operator)
2. “New Style” String Formatting (str.format)
3. String Interpolation / f-Strings (Python 3.6+)
4. Template Strings (Standard Library)

---

# Regler for hvilken metode en bør bruke:

1. Python String Formatting Rule of Thumb: If your format strings are user-supplied, use Template Strings (#4) to avoid security issues.
2. Otherwise, use Literal String **Interpolation/f-Strings (#3)** if you’re on Python 3.6+, and “New Style” str.format (#2) if you’re not.
