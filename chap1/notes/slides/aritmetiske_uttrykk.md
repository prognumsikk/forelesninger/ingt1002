---
marp: true
size: 4:3
paginate: true
---
<!-- https://marpit.marp.app/directives -->

# Aritmetiske operasjoner i Python

[Aritmetiske operasjoner i Python](https://www.w3schools.com/python/python_operators.asp)

![w:400 h:300](images/aritmetiske_operasjoner.png)

__Viktig å skrive aritmetiske uttrykk riktig!__
