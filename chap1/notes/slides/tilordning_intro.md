---
marp: true
size: 4:3
paginate: true
---
<!-- https://marpit.marp.app/directives -->

# Tilordninger i Python

[Variabler er beholdere for lagring av dataverdier](https://www.w3schools.com/python/python_variables.asp)

Vi tilordner variabler dataverdier.

![Alt text](images/image.png)

<variable> = <data>

---

# Hvorfor gjør vi det?

- Vi lagrer verdier i minne
- Vi finner fram verdien ved å bruke variabelen

![Alt text](images/variable.png)

