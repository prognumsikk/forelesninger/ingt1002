---
marp: true
size: 4:3
paginate: true
---
<!-- https://marpit.marp.app/directives -->

# Leksjon 1

**Læringsutbytte**

* Intro til emnet 
    – Hva skal dere lære? 
    – Hvorfor? 
    – Hvordan? 
    – Invitasjon til referansegruppe
* Intro til Python 
    – Aritmetiske uttrykk 
    – Variable og tilordning, datatyper
    – Litt om funksjoner

 ---
 # __Hva__ skal dere lære?

* Faglig innhold
    - Se emnets nettside: https://www.ntnu.no/studier/emner/INGT1002
    - Emnet består av 3 deler
        - Programmering
        - Numerikk
        - IKT Sikkerhet
---    
# __Hvorfor__ lære dette?

* Programmering er for __alle__
    - https://code.org/quotes
* Nyttig både videre i studiet, og i yrkeslivet
* Programmering brukes over alt
    - Styre roboter, kontrollsystemer, maskiner, skip
    - Design og utvikling av produkter, utvinning av naturressurser
    - Vitenskapelige beregninger, simuleringer

---
* Python er fleksibelt og utvidbart
    - Mange moduler for ulike behov (numerikk, plotting, AI)
    - Relativt lav terskel som nybegynnerspråk
    - Ulempe: Ikke spesielt raskt, men kan kalle kode i C og FORTRAN hvis nødvendig

---
# Relevante eksempler

---
# De meste populære Python pakkene

---
# Hvordan lære dette?

---
# Introduksjon til Python

* Hva bør du lære denne uka?
– Hvordan skrive aritmetiske uttrykk i Python
– Variable: tilordning og bruk. Hvordan virker =
– Hvordan definere enkle funksjoner
– Hvordan lage enkle plott av funksjoner
– Enkel inn/ut av tekst med input() og print()
– Mikse fast og variabel tekst med f-strenger
* Gå til Blackboard for å finne ressurser for å lære dette
    - Se videoer
    - Les / jobb deg gjennom Jupyter Notebooks