---
marp: true
size: 4:3
paginate: true
---
<!-- https://marpit.marp.app/directives -->

# Program

[Et sett med instruksjoner til datamaskinen](https://snl.no/dataprogram)


![Alt text](images/program.png)

<variable> = <data>

---

# Variable

[Variabler er beholdere for lagring av dataverdier](https://www.w3schools.com/python/python_variables.asp)

Vi tilordner variabler dataverdier.

![Alt text](images/image.png)

