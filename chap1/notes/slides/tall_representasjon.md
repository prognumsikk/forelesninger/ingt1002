---
marp: true
size: 4:3
paginate: true
---
<!-- https://marpit.marp.app/directives -->

# Leksjon 1 - Tall representasjon

Vanlig tallsystem har base 10. 
Posisjonen til sifrene avgjør hvilken tierpotens de representerer. 
F.eks.
- 234 betyr $2\cdot10^2 + 3\cdot10^1 + 4\cdot10^0 = 200 + 30 + 4$
- 0.567 betyr $5\cdot10^{-1} + 6\cdot10^{-2} + 7\cdot10^{-3} = 0.5 + 0.06 + 0.007$

---
I datamaskinen brukes det binære tallsystemet, med base 2, hvor kun sifrene 0 og 1 benyttes. 

Vi vil her ta for oss:
- binær representasjon av heltall
- binær representasjon av flyttall

---
# Binær representasjon av heltall
I Python kan vi representere 
- så lange heltall som helst, og 
- heltall kan alltid representeres eksakt. 

---
# Binær representasjon av flyttall
- representeres som brøker av heltall
    - f.eks. 0.567 er implisitt brøken 567/1000
- maskinen bruker base 2
    - nevner må være en ren toerpotens
- metoden __as_integer_ratio()__ 
    - viser heltallsbrøken et tall er representert som 

---
Enkelte flyttall kan ikke vises nøyaktig i datamaskinen

F.eks.
0.1  = 1/10. 
Nevner er 10 og er ikke toerpotens (2*5), dvs. kan ikke skrives som 2**n, der n kan være 0, 1, ...
