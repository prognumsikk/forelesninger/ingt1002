---
marp: true
size: 4:3
paginate: true
---
<!-- https://marpit.marp.app/directives -->

# Datatyper

Du kan lese mer om Python datatyper på [W3schools](https://www.w3schools.com/python/python_datatypes.asp), [GeeksforGeeks](https://www.geeksforgeeks.org/python-data-types/) og/eller [TutorialsPoint](https://www.tutorialspoint.com/What-are-different-data-conversion-methods-in-Python)

![w:600 h:400](images/datatypes.png)

---

# Datatyper

- For hver type data reserveres et vist antall mengde med bytes.
- Vi kan konvertere mellom datatyper
- Vi skiller mellom implisitt og eksplisitt type konvertering
    - Python endrer automatisk et objekts datatype uten programmererens inngripen
    - Programmerer endrer et objekts datatype


