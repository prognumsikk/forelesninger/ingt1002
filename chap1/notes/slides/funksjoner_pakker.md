---
marp: true
size: 4:3
paginate: true
---
<!-- https://marpit.marp.app/directives -->

# Funksjoner

Vi kan lage egne funksjoner eller bruke eksisterende funksjoner

[En funksjon er en blokk med kode som defineres en gang og kalles flere ganger](https://www.w3schools.com/python/python_functions.asp)


![Alt text](images/funksjon.png)


---

# Eksisterende funksjoner 

- Innebygde funksjoner (følger med Python)
- Funksjoner fra pakker (biblioteker)

---
# Innebygde funksjoner

[Python Built-in Functions](https://docs.python.org/3/library/functions.html)

- print()
- input()
- abs()
- sum()
...

---

# The Python Standard Library

[The Python Standard Library](https://docs.python.org/3/library/)
[Database over tilgjengelige pakker](https://pypi.org/)

# Python modules

- En pakke består av en eller flere moduler
- En modul er en python fil

F.eks. math

[math — Mathematical functions](https://docs.python.org/3/library/math.html)
