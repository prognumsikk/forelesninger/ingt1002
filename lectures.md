<details><summary style="font-size:200%;cursor: pointer;">INGT1002 Programmering, numerikk og IKT sikkerhet</summary>
<blockquote style="font-size:200%;cursor: pointer;padding: 10px;">&emsp;<a target="_blank" href=https://www.ntnu.no/studier/emner/INGT1002>Faginnhold og læringsutbytte beskrivelser</a></blockquote>
<blockquote style="font-size:200%;cursor: pointer;padding: 10px;">&emsp;<a target="_blank" href=https://ntnu.blackboard.com/webapps/blackboard/content/listContentEditable.jsp?content_id=_2130256_1&course_id=_43000_1>Pensum og litteratur</a></blockquote>
<details><summary style="font-size:200%;cursor: pointer;">Innholdsfortegnelse</summary>
<details><summary style="font-size:200%;cursor: pointer;">&emsp;Kapittel 1: Introduksjon, variabler og datatyper, flyttall og presisjon, enkle funksjoner</summary>
<details><summary style="font-size:200%;cursor: pointer;">&emsp;&emsp;Introduksjon</summary>
<blockquote style="font-size:200%;cursor: pointer;padding: 10px;">&emsp;&emsp;&emsp;<a target="_blank" href=chap1/notes/slides/introduksjon.md>Motivasjon</a></blockquote>
<blockquote style="font-size:200%;cursor: pointer;padding: 10px;">&emsp;&emsp;&emsp;<a target="_blank" href=chap1/README.md>Kilder og referanser</a></blockquote>
<blockquote style="font-size:200%;cursor: pointer;padding: 10px;">&emsp;&emsp;&emsp;<a target="_blank" href=chap1/notes/codes/final/intro_til_jupyter.ipynb>Komme i gang med Jupyter Notebook</a></blockquote>
</details>
<details><summary style="font-size:200%;cursor: pointer;">&emsp;&emsp;Komme i gang med Python</summary>
<blockquote style="font-size:200%;cursor: pointer;padding: 10px;">&emsp;&emsp;&emsp;<a target="_blank" href=chap1/notes/codes/final/variable.ipynb>Introduksjon til variabler</a></blockquote>
<blockquote style="font-size:200%;cursor: pointer;padding: 10px;">&emsp;&emsp;&emsp;<a target="_blank" href=chap1/notes/codes/final/aritmetiske_uttrykk.ipynb>Aritmetiske uttrykk</a></blockquote>
<blockquote style="font-size:200%;cursor: pointer;padding: 10px;">&emsp;&emsp;&emsp;<a target="_blank" href=chap1/notes/codes/final/tilordning_intro.ipynb>Tilordninger intro</a></blockquote>
<blockquote style="font-size:200%;cursor: pointer;padding: 10px;">&emsp;&emsp;&emsp;<a target="_blank" href=chap1/notes/codes/final/tilordning_objekt_ref.ipynb>Tilordninger objekt referanser</a></blockquote>
<blockquote style="font-size:200%;cursor: pointer;padding: 10px;">&emsp;&emsp;&emsp;<a target="_blank" href=chap1/notes/codes/final/syntax_error.ipynb>SyntaxError</a></blockquote>
<blockquote style="font-size:200%;cursor: pointer;padding: 10px;">&emsp;&emsp;&emsp;<a target="_blank" href=chap1/notes/codes/final/name_error.ipynb>NameError</a></blockquote>
</details>
<details><summary style="font-size:200%;cursor: pointer;">&emsp;&emsp;Variabler og datatyper</summary>
<blockquote style="font-size:200%;cursor: pointer;padding: 10px;">&emsp;&emsp;&emsp;<a target="_blank" href=chap1/notes/codes/final/variable.ipynb>Introduksjon</a></blockquote>
<blockquote style="font-size:200%;cursor: pointer;padding: 10px;">&emsp;&emsp;&emsp;<a target="_blank" href=chap1/notes/codes/final/datatyper.ipynb>Datatyper</a></blockquote>
<blockquote style="font-size:200%;cursor: pointer;padding: 10px;">&emsp;&emsp;&emsp;<a target="_blank" href=chap1/notes/codes/final/typekonvertering.ipynb>Type konvertering</a></blockquote>
</details>
<details><summary style="font-size:200%;cursor: pointer;">&emsp;&emsp;Flyttall og presisjon</summary>
<blockquote style="font-size:200%;cursor: pointer;padding: 10px;">&emsp;&emsp;&emsp;<a target="_blank" href=chap1/notes/codes/final/tallrepresentasjon.ipynb>Tallrepresentasjon</a></blockquote>
<blockquote style="font-size:200%;cursor: pointer;padding: 10px;">&emsp;&emsp;&emsp;<a target="_blank" href=chap1/notes/codes/final/avrundingsfeil.ipynb>Avrundingsfeil</a></blockquote>
</details>
<details><summary style="font-size:200%;cursor: pointer;">&emsp;&emsp;Introduksjon til funksjoner</summary>
<blockquote style="font-size:200%;cursor: pointer;padding: 10px;">&emsp;&emsp;&emsp;<a target="_blank" href=chap1/notes/codes/final/intro_funk_pakker.ipynb>Enkle funksjoner</a></blockquote>
</details>
</details>
<details><summary style="font-size:200%;cursor: pointer;">&emsp;Kapittel 2: Funksjoner og pakker, logiske uttrykk, betingelser</summary>
<details><summary style="font-size:200%;cursor: pointer;">&emsp;&emsp;Mer om funksjoner</summary>
<blockquote style="font-size:200%;cursor: pointer;padding: 10px;">&emsp;&emsp;&emsp;<a target="_blank" href=chap2/notes/codes/final/mer_funksjoner.ipynb>Funksjoner og kalkulasjoner</a></blockquote>
</details>
<details><summary style="font-size:200%;cursor: pointer;">&emsp;&emsp;Pakker og moduler</summary>
<blockquote style="font-size:200%;cursor: pointer;padding: 10px;">&emsp;&emsp;&emsp;<a target="_blank" href=chap2/notes/codes/final/mer_moduler.ipynb>Pakker og moduler</a></blockquote>
</details>
<details><summary style="font-size:200%;cursor: pointer;">&emsp;&emsp;Logiske uttrykk</summary>
<blockquote style="font-size:200%;cursor: pointer;padding: 10px;">&emsp;&emsp;&emsp;<a target="_blank" href=chap2/notes/codes/final/logiske_uttrykk.ipynb>Logiske operasjoner</a></blockquote>
</details>
<details><summary style="font-size:200%;cursor: pointer;">&emsp;&emsp;Betingelser</summary>
<blockquote style="font-size:200%;cursor: pointer;padding: 10px;">&emsp;&emsp;&emsp;<a target="_blank" href=chap2/notes/codes/final/betingelser.ipynb>if-setninger</a></blockquote>
</details>
</details>
<details><summary style="font-size:200%;cursor: pointer;">&emsp;Kapittel 3: Løkker</summary>
<details><summary style="font-size:200%;cursor: pointer;">&emsp;&emsp;For/While-løkker</summary>
<blockquote style="font-size:200%;cursor: pointer;padding: 10px;">&emsp;&emsp;&emsp;<a href='chap3/notes/codes/lokker.ipynb'>For/while-løkke</a></blockquote>
</details>
<details><summary style="font-size:200%;cursor: pointer;">&emsp;&emsp;Tallserier og løkker</summary>
<blockquote style="font-size:200%;cursor: pointer;padding: 10px;">&emsp;&emsp;&emsp;<a href='chap3/notes/codes/lokker_tallserier.ipynb'>Tallserier og løkker</a></blockquote>
</details>
</details>
<details><summary style="font-size:200%;cursor: pointer;">&emsp;Kapittel 4: Sammensatte datatyper (lister, tupler, set)</summary>
<details><summary style="font-size:200%;cursor: pointer;">&emsp;&emsp;Sammensatte datatyper</summary>
<details><summary style="font-size:200%;cursor: pointer;">&emsp;&emsp;&emsp;Lister (array)</summary>
<blockquote style="font-size:200%;cursor: pointer;padding: 10px;">&emsp;&emsp;&emsp;&emsp;<a href='chap4/notes/codes/list_array.ipynb'>List array</a></blockquote>
</details>
<details><summary style="font-size:200%;cursor: pointer;">&emsp;&emsp;&emsp;Lister slicing</summary>
<blockquote style="font-size:200%;cursor: pointer;padding: 10px;">&emsp;&emsp;&emsp;&emsp;<a href='chap4/notes/codes/list_slicing.ipynb'>List slicing</a></blockquote>
</details>
<details><summary style="font-size:200%;cursor: pointer;">&emsp;&emsp;&emsp;Tupler</summary>
<blockquote style="font-size:200%;cursor: pointer;padding: 10px;">&emsp;&emsp;&emsp;&emsp;<a href='chap4/notes/codes/tuples.ipynb'>Tupler</a></blockquote>
</details>
<details><summary style="font-size:200%;cursor: pointer;">&emsp;&emsp;&emsp;Set</summary>
<blockquote style="font-size:200%;cursor: pointer;padding: 10px;">&emsp;&emsp;&emsp;&emsp;<a href='chap4/notes/codes/sets.ipynb'>Sets</a></blockquote>
</details>
</details>
</details>
<details><summary style="font-size:200%;cursor: pointer;">&emsp;Kapittel 5: Numerisk derivasjon, integrasjon og diff.ligninger</summary>
<details><summary style="font-size:200%;cursor: pointer;">&emsp;&emsp;Mumerisk derivasjon</summary>
<details><summary style="font-size:200%;cursor: pointer;">&emsp;&emsp;&emsp;Numerisk derivasjon</summary>
<blockquote style="font-size:200%;cursor: pointer;padding: 10px;">&emsp;&emsp;&emsp;&emsp;<a target="_blank" href=chap5/notes/codes/derivasjon/numerisk_derivasjon.ipynb>Introduksjon</a></blockquote>
</details>
<details><summary style="font-size:200%;cursor: pointer;">&emsp;&emsp;&emsp;Backward</summary>
<blockquote style="font-size:200%;cursor: pointer;padding: 10px;">&emsp;&emsp;&emsp;&emsp;<a target="_blank" href=chap5/notes/codes/derivasjon/numerisk/eks1/eks1_backward.py>Eksempel 1</a></blockquote>
<blockquote style="font-size:200%;cursor: pointer;padding: 10px;">&emsp;&emsp;&emsp;&emsp;<a target="_blank" href=chap5/notes/codes/derivasjon/numerisk/eks2/eks2_backward.py>Eksempel 2</a></blockquote>
</details>
<details><summary style="font-size:200%;cursor: pointer;">&emsp;&emsp;&emsp;Forward</summary>
<blockquote style="font-size:200%;cursor: pointer;padding: 10px;">&emsp;&emsp;&emsp;&emsp;<a target="_blank" href=chap5/notes/codes/derivasjon/numerisk/eks1/eks1_forward.py>Eksempel 1</a></blockquote>
<blockquote style="font-size:200%;cursor: pointer;padding: 10px;">&emsp;&emsp;&emsp;&emsp;<a target="_blank" href=chap5/notes/codes/derivasjon/numerisk/eks2/eks2_forward.py>Eksempel 2</a></blockquote>
<blockquote style="font-size:200%;cursor: pointer;padding: 10px;">&emsp;&emsp;&emsp;&emsp;<a target="_blank" href=chap5/notes/codes/derivasjon/numerisk/eks3/eks3_forward.py>Eksempel 3</a></blockquote>
</details>
<details><summary style="font-size:200%;cursor: pointer;">&emsp;&emsp;&emsp;Sentral</summary>
<blockquote style="font-size:200%;cursor: pointer;padding: 10px;">&emsp;&emsp;&emsp;&emsp;<a target="_blank" href=chap5/notes/codes/derivasjon/numerisk/eks1/eks1_sentral.py>Eksempel 1</a></blockquote>
<blockquote style="font-size:200%;cursor: pointer;padding: 10px;">&emsp;&emsp;&emsp;&emsp;<a target="_blank" href=chap5/notes/codes/derivasjon/numerisk/eks2/eks2_sentral.py>Eksempel 2</a></blockquote>
</details>
<details><summary style="font-size:200%;cursor: pointer;">&emsp;&emsp;&emsp;Alt samlet</summary>
<blockquote style="font-size:200%;cursor: pointer;padding: 10px;">&emsp;&emsp;&emsp;&emsp;<a target="_blank" href=chap5/notes/codes/derivasjon/numerisk/eks1/eks1_all.py>Eksempel 1</a></blockquote>
</details>
</details>
<details><summary style="font-size:200%;cursor: pointer;">&emsp;&emsp;Mumerisk integrasjon</summary>
<details><summary style="font-size:200%;cursor: pointer;">&emsp;&emsp;&emsp;Numerisk integrasjon</summary>
<blockquote style="font-size:200%;cursor: pointer;padding: 10px;">&emsp;&emsp;&emsp;&emsp;<a target="_blank" href=chap5/notes/codes/integrasjon/numerisk_integrasjon.ipynb>Introduksjon</a></blockquote>
</details>
<details><summary style="font-size:200%;cursor: pointer;">&emsp;&emsp;&emsp;Rectangle</summary>
<blockquote style="font-size:200%;cursor: pointer;padding: 10px;">&emsp;&emsp;&emsp;&emsp;<a target="_blank" href=chap5/notes/codes/integrasjon/eks1_rektangel.py>Eksempel</a></blockquote>
</details>
<details><summary style="font-size:200%;cursor: pointer;">&emsp;&emsp;&emsp;Trapes</summary>
<blockquote style="font-size:200%;cursor: pointer;padding: 10px;">&emsp;&emsp;&emsp;&emsp;<a target="_blank" href=chap5/notes/codes/integrasjon/eks1_trapes.py>Eksempel</a></blockquote>
</details>
<details><summary style="font-size:200%;cursor: pointer;">&emsp;&emsp;&emsp;Analytisk /(lib)</summary>
<blockquote style="font-size:200%;cursor: pointer;padding: 10px;">&emsp;&emsp;&emsp;&emsp;<a target="_blank" href=chap5/notes/codes/integrasjon/eks1_lib.py>Eksempel</a></blockquote>
</details>
<details><summary style="font-size:200%;cursor: pointer;">&emsp;&emsp;&emsp;Datafil</summary>
<blockquote style="font-size:200%;cursor: pointer;padding: 10px;">&emsp;&emsp;&emsp;&emsp;<a target="_blank" href=chap5/notes/codes/integrasjon/eks1_datafil.py>Eksempel</a></blockquote>
</details>
<details><summary style="font-size:200%;cursor: pointer;">&emsp;&emsp;&emsp;Alt samlet</summary>
<blockquote style="font-size:200%;cursor: pointer;padding: 10px;">&emsp;&emsp;&emsp;&emsp;<a target="_blank" href=chap5/notes/codes/integrasjon/integrasjon_all.py>Eksempel</a></blockquote>
</details>
</details>
<details><summary style="font-size:200%;cursor: pointer;">&emsp;&emsp;Differensial ligninger</summary>
<details><summary style="font-size:200%;cursor: pointer;">&emsp;&emsp;&emsp;Eulers metode</summary>
<blockquote style="font-size:200%;cursor: pointer;padding: 10px;">&emsp;&emsp;&emsp;&emsp;</blockquote>
</details>
</details>
</details>
