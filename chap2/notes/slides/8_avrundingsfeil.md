---
marp: true
size: 4:3
paginate: true
---
<!-- https://marpit.marp.app/directives -->

# Leksjon 1 - Avrundingsfeil

Mange tall lar seg ikke representere eksakt. Dette gjelder
- irrasjonale tall som $\pi$ og $\sqrt{2}$. 
    - kan ikke representeres eksakt som brøker av heltall
    - uendelig mange desimaler uansett hvilket tallsystem vi velger

---
- rasjonale tall som __kunne__ ha vært representert eksakt som en brøk av heltall...
    - men __ikke__ i det binære tallsystemet, hvor nevneren i brøken må være en toerpotens. 
    - Kun brøker hvor nevneren er 2, 4, 8, 16, 32, ... blir eksakte i dette tallsystemet
        - mens f.eks. 0.1 (1/10) må representeres med en tilnærmet verdi, med avrundingsfeil

---
# Konsekvenser

Konsekvensene av avrundingsfeil kan være store

Noen områder fra virkeligheten:

- Finansielle beregninger
- Vitenskapelige målinger
- Aksjehandel
- Statistisk analyse
- Produksjon oh ingeniør virksomhet
- Valutakonvertering
- GPS-baserte treningsmålere
- Vitenskapelige simuleringer
- Skatteberegninger
- ...

---
# Enkel eksempel

I slutten av februar 1991, under Golfkrigen, traff en irakisk Scud-missil amerikanske brakker i Dhahran, Saudi-Arabia, og drepte 28 soldater og såret 260.

- Det grunnleggende problemet lå i __kvantiseringen av faktoren__ som ble brukt til å __konvertere timingvariabelen__ til en intern klokke (representert som et heltall, i tidels sekunder)

---

- Når en tidels sekund representeres i binære tall, blir den 0.00011001100110011, med 0011 som gjentar seg om og om igjen. 

- Når dette blir avkortet til 24 bits - altså antall plasser som kan brukes i representasjonen - oppstår det en feil på 0,000000095.

---
Resultatet ble en feil på 573 meter. Patriot-systemet trodde at Scud-missilet var langt unna brakkene og avfyrte ikke.

---
# Situasjoner som kan føre til avrundingsfeil

- Summere tall av svært ulik størrelse
- Subtraksjon av to nesten like tall

---
# Overflyt

__Overflyt:__ tallet blir for stort til at det kan reprensenteres med den typen flyttall vi bruker

Overflyt kan typisk forekomme ved
    - multiplikasjon av to store tall
    - divisjon av et stort tall på et lite tall (mye mindre enn 1) 


---
# Underflyt

__Underflyt:__ tallet blir for lite til at det kan representeres

Underflyt kan typisk forekomme ved 
- multiplikasjon av to små tall
- divisjon med lite tall over stort tall

