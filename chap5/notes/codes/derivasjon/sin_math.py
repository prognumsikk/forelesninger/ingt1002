
import matplotlib.pyplot as plt
import numpy as np
from math import sin


def f(x):
    '''Function to differentiate
    '''
    return sin(x)


def forward_derivative(x, dx):
    '''Forward difference approximation of the derivative'''
    return (f(x + dx) - f(x))/dx


def backward_derivative(x, dx):
    '''Backward difference approximation of the derivative'''
    return f(x)-(f(x - dx))/dx


def central_derivative(x, dx):
    '''Central difference approximation of the derivative'''
    return (f(x + dx) - f(x-dx))/(2*dx)


# Main program starts here
# Define the interval and the step size
dx = 1E-8
x = np.linspace(-2, 2, 100)

# Calculate the function values
y = [f(i) for i in x]

# Calculate the derivative values
y_forward = [forward_derivative(i, dx) for i in x]
y_backward = [backward_derivative(i, dx) for i in x]
y_central = [central_derivative(i, dx) for i in x]

# Plot the function and the derivative
plt.figure(num=1, figsize=(24/2.54, 18/2.54))  # Inch

plt.subplot(3, 1, 1)
plt.plot(x, y, 'r', label='f(x)')
plt.plot(x, y_forward, 'b-', label="f'(x) - forward")
plt.grid(which='both', color='grey')
plt.legend()

plt.subplot(3, 1, 2)
plt.plot(x, y, 'r', label='f(x)')
plt.plot(x, y_backward, 'b-', label="f'(x) - backward")
plt.grid(which='both', color='grey')
plt.legend()

plt.subplot(3, 1, 3)
plt.plot(x, y, 'r', label='f(x)')
plt.plot(x, y_central, 'b-', label="f'(x) - central")
plt.grid(which='both', color='grey')
plt.legend()

plt.show()
