# Source: https://progmod.no/docs/tema4_algoritmer/derivasjon.html

import matplotlib.pyplot as plt
import pandas as pd

# Leser og sjekker ut dataene
data = pd.read_csv("https://raw.githubusercontent.com/andreasdh/NAT3000/master/docs/datafiler/posisjon.txt")

print(data.head())

x = data["tid_s"]
y = data["posisjon_m"]


dydx = []
for i in range(len(y)-1):
    dy = y[i+1]-y[i]
    dx = x[i+1]-x[i]
    dydx.append(dy/dx)

dydx.append(None)
plt.plot(x, y, label='Posisjon (m)')
plt.plot(x, dydx, label='Fart (m/s)')
plt.xlabel('Tid (s)')
plt.legend()
plt.show()
