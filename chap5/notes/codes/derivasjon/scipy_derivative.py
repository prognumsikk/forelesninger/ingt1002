from scipy.misc import derivative
from math import sqrt, log

# define functions


def f1(x):
    return x**2 - 4


def f2(x):
    return x**2 - 4*x + 5


def f3(x):
    return sqrt(log(x))

# Use derivative function from scipy.misc to derivate f1 for x=1


derivert = derivative(f1, 1)
print(f"f'(1) = {derivert}")

# Use derivative function from scipy.misc to derivate f2 for x=1
derivert = derivative(f2, 1)
print(f"f'(1) = {derivert}")

# Use derivative function from scipy.misc to derivate f3 for x=1
derivert = derivative(f3, 1)
print(f"f'(1) = {derivert}")
