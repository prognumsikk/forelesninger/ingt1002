import scipy.integrate as integrate


def f(x):  # Definerer en funksjon som vi skal integrere.
    return x**2 - 2*x + 2

def midtpunkt_metoden(f, a, b, n):
    h = (b-a) / n    
    
    integral = 0
    for k in range(n):
        x_k = a + (k + 0.5) * h
        integral += f(x_k) * h    
    return integral    

def rektangel_metoden(f, a, b, n):
    integral = 0.0
    h = (b-a)/n  # Bredden til rektanglene
    for k in range(n):
        x_k = a + k*h
        integral = integral + f(x_k)*h
    return integral


def trapes_metoden(f, a, b, n):
    h = (b-a)/n
    integral = (f(a) + f(b))/2.0
    for k in range(1, n):
        x_k = a + k*h
        integral = integral + f(x_k)
    return integral*h


def simpsons_metoden(f, a, b, n):
    h = (b-a)/n
    integral = f(a) + f(b)
    
    for k in range(1, n, 2):
        x_k = a + k*h
        integral += 4 * f(x_k)
        
    for k in range(2, n, 2):
        x_k = a + k*h
        integral += 2*f(x_k)
        
    return integral*h/3

fra = 1
til = 4
n = 1000    

print("Numerisk verdi (midtpunkt):", midtpunkt_metoden(f, fra, til, n))
print("Numerisk verdi (rektangel):", rektangel_metoden(f, fra, til, n))
print("Numerisk verdi (trapes):", trapes_metoden(f, fra, til, n))
print("Numerisk verdi (simpsons):", simpsons_metoden(f, fra, til, n))
print("Analytisk verdi:", integrate.quad(f, fra, til))
